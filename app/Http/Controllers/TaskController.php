<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteTaskRequest;
use App\Http\Requests\SaveTaskRequest;
use App\NN\Exceptions\TaskValidationException;
use App\NN\Exceptions\ToDoListException;
use App\NN\Interfaces\Task\ToDoListTaskHelperInterface;
use App\NN\Interfaces\Task\ToDoListTaskRepositoryInterface;

/**
 * Handle task related requests
 * Class TaskController
 * @package App\Http\Controllers
 */
class TaskController extends Controller
{

    /** @var ToDoListTaskRepositoryInterface */
    protected $repo;

    /** @var ToDoListTaskHelperInterface */
    protected $helper;

    public function __construct(ToDoListTaskRepositoryInterface $repository, ToDoListTaskHelperInterface $helper)
    {
        $this->repo = $repository;
        $this->helper = $helper;
    }


    /**
     * Store a newly created resource in storage.
     * @param SaveTaskRequest $request
     * @return \Illuminate\Http\Response
     */
    public function save(SaveTaskRequest $request) {
        try {

            $this->helper->updateOrCreate($request->all());

            return response()->json(true);
        } catch (ToDoListException | TaskValidationException $toDoListException) {

            return response()->json(['error' => $toDoListException->getMessage()]);
        } catch (\Exception $exception) {

            return response()->json(['error' => "Error during task operation please try again."]);
        }
    }


    /**
     * Remove the specified resource from storage.
     * @param DeleteTaskRequest $request
     * @return \Illuminate\Http\Response
     */
    public function delete(DeleteTaskRequest $request) {
        try {

            $task = $this->repo->getById($request->get('id'));
            $this->repo->delete($task);

            return response()->json(true);
        } catch (ToDoListException $toDoListException) {

            return response()->json(['error' => $toDoListException->getMessage()]);
        } catch (\Exception $exception) {

            return response()->json(['error' => "Error during task deletion please try again."]);
        }

    }
}
