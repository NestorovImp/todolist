<?php

namespace App\Http\Controllers;

use App\Http\Requests\DeleteListRequest;
use App\Http\Requests\MarkTaskCompleteRequest;
use App\Http\Requests\SaveListRequest;
use App\NN\Exceptions\TaskValidationException;
use App\NN\Exceptions\ToDoListException;
use App\NN\Interfaces\Task\ToDoListTaskRepositoryInterface;
use App\NN\Interfaces\ToDoList\ToDoListHelperInterface;
use App\NN\Interfaces\ToDoList\ToDoListRepositoryInterface;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;

/**
 * Handle ToDoList related requests
 * Class ToDoListController
 * @package App\Http\Controllers
 */
class ToDoListController extends BaseController
{
    /** @var ToDoListRepositoryInterface */
    protected $repo;
    /** @var ToDoListTaskRepositoryInterface */
    protected $tasksRepo;

    /** @var ToDoListHelperInterface */
    protected $helper;

    /**
     * ToDoListController constructor.
     * @param ToDoListRepositoryInterface $repository
     * @param ToDoListTaskRepositoryInterface $tasksRepo
     * @param ToDoListHelperInterface $helper
     */
    public function __construct(ToDoListRepositoryInterface $repository, ToDoListTaskRepositoryInterface $tasksRepo, ToDoListHelperInterface $helper)
    {
        $this->repo = $repository;
        $this->tasksRepo = $tasksRepo;
        $this->helper = $helper;
    }

    /**
     * Show list of ToDoList objects
     */
    public function index() {

        $lists = $this->repo->getAll();

        return view('lists.index', compact('lists'));

    }

    /**
     * Showing edit/create screen for ToDoList entity
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function edit($id = null) {

        try {

            $list = !empty($id) ? $this->repo->getById($id) : null;

            $tasks = !empty($id) ? $this->tasksRepo->getTaskForListById($id) : [];

        } catch (ToDoListException $toDoListException) {

            return redirect()->route('list.index.get')->withErrors($toDoListException->getMessage());
        } catch (\Exception $exception) {

            return redirect()->route('list.index.get')->withErrors("Error occurred please try again later.");
        }

        return view('lists.edit', compact('list', 'tasks'));
    }

    /**
     * Store or update a ToDoLiSt in storage.
     * @param SaveListRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function save(SaveListRequest $request) {

        try{
            DB::beginTransaction();

            $list = $this->helper->updateOrCreate($request->all());

            DB::commit();

            return redirect()->route('list.edit.get', ['id' => $list->getId()])->with('successMsg', "Success");
        } catch (ToDoListException $toDoListException) {
            DB::rollBack();

            return redirect()->route('list.index.get')->withErrors($toDoListException->getMessage());
        } catch (\Exception $exception) {
            DB::rollBack();

            return redirect()->route('list.index.get')->withErrors("Error occurred please try again later.");
        }
    }


    /**
     * Deleting ToDoList and ToDoLisTaks assigned to it
     * @param DeleteListRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(DeleteListRequest $request) {

        try {
            DB::beginTransaction();

            $list = $this->repo->getById($request->get('id'));

            $this->tasksRepo->deleteByTodoListId($list->id);
            $this->repo->delete($list);

            DB::commit();

            return redirect()->route('list.index.get')->with('successMsg', "Success");
        } catch (\Exception $exception) {
            DB::rollBack();

            return redirect()->route('task.index.get')->withErrors("Error while deleting and list please try again.");
        }

    }

    /**
     * Mark task of corresponding list completed
     * @param MarkTaskCompleteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function markTaskCompleted(MarkTaskCompleteRequest $request) {

        try {
            DB::beginTransaction();

            $this->helper->markTaskCompleted($request->all());

            DB::commit();

            return response()->json(true);
        }  catch (ToDoListException | TaskValidationException $toDoListException) {
            DB::rollBack();

            return response()->json(['error' => $toDoListException->getMessage()]);
        } catch (\Exception $exception) {
            DB::rollBack();

            return response()->json(['error' => "Error during task deletion please try again."]);
        }

    }
}
