<?php

namespace App\NN\Helpers;


use App\Models\ToDoList;
use App\NN\Interfaces\ToDoList\ToDoListHelperInterface;
use App\NN\Interfaces\ToDoList\ToDoListInterface;
use App\NN\Interfaces\ToDoList\ToDoListRepositoryInterface;
use App\NN\Interfaces\Task\ToDoListTaskHelperInterface;
use App\NN\Interfaces\Task\ToDoListTaskRepositoryInterface;
use App\NN\Services\Validation\Rules\TaskCompleted;
use App\NN\Services\Validation\Rules\TaskIsNotPastOverdue;
use App\NN\Services\Validation\Rules\TaskNotCompleted;
use App\NN\Services\Validation\Rules\TaskNotDisabled;
use App\NN\Services\Validation\Validators\TaskValidator;

/**
 * Helper service for manipulating ToDoList entity attributes
 * Class ToDoListHelper
 * @package App\NN\Helpers
 */
class ToDoListHelper implements ToDoListHelperInterface
{

    /** @var ToDoListRepositoryInterface */
    protected $listsRepo;

    /** @var ToDoListTaskRepositoryInterface */
    protected $tasksRepo;

    /** @var ToDoListTaskHelperInterface */
    protected $taskHelper;

    /**
     * ToDoListHelper constructor.
     * @param ToDoListRepositoryInterface $listsRepo
     * @param ToDoListTaskRepositoryInterface $tasksRepo
     * @param ToDoListTaskHelperInterface $taskHelper
     */
    public function __construct(ToDoListRepositoryInterface $listsRepo, ToDoListTaskRepositoryInterface $tasksRepo, ToDoListTaskHelperInterface $taskHelper)
    {
        $this->listsRepo = $listsRepo;
        $this->tasksRepo = $tasksRepo;
        $this->taskHelper = $taskHelper;
    }

    /**
     * Filling ToDoList entity attributes
     * @param ToDoListInterface $list
     * @param $data
     * @return ToDoListInterface
     */
    public function fill(ToDoListInterface $list, $data)  {

        if(!empty($data)) {
            $list->setTitle($data['title'] ?? null);
            $list->setDescription($data['description'] ?? null);
        }

        return $list;
    }

    /**\
     * Update existing entity attributes or create a new one
     * @param $data
     * @return ToDoList|ToDoListInterface
     */
    public function updateOrCreate($data) {

        $id = $data['id'] ?? null;

        $list = !empty($id) ? $this->listsRepo->getById($id) : resolve(ToDoListInterface::class);

        $list = $this->fill($list, $data ?? []);

        $this->listsRepo->save($list);

        $tasks = $data['tasks'] ?? [];

        $attachedTasks = is_null($id) ? $this->attachTasks($tasks, $list) : null;

        return $list;
    }

    /**
     * Attaching related to ToDoList tasks
     * @param $tasks
     * @param ToDoListInterface $list
     * @return array
     */
    public function attachTasks($tasks, ToDoListInterface $list) {
        $attachedTasks = [];

        foreach ($tasks as $task) {
            $task['list_id'] = $list->getID();
            $attachedTasks[] = $this->taskHelper->updateOrCreate($task);
        }

        return $attachedTasks;
    }

    /**
     * Marking related to ToDoList task as completed
     * @param $data
     * @return bool
     */
    public function markTaskCompleted($data) {

        $completed = $data['completed'];

        $list = $this->listsRepo->getById($data['listId']);

        $task = $this->tasksRepo->getByIdAndListId($data['taskId'], $list->getID());

        $validator = new TaskValidator($task);

        if($validator->validate(new TaskIsNotPastOverdue(), new TaskNotDisabled(), ($completed) ? new TaskCompleted() : new TaskNotCompleted())) {
            $task->setCompleted($completed);
        }
        $this->tasksRepo->save($task);


        $this->markListCompleted($list);

        return true;

    }

    /**
     * Marking ToDoList as completed
     * @param ToDoListInterface $list
     */
    public function markListCompleted(ToDoListInterface $list) {
        if($list->isAllTasksCompleted()) {
            $list->markCompleted();
        } else {
            $list->setCompleted(false);
            $list->setCompletedAt(null);
        }
        $this->listsRepo->save($list);
    }




}