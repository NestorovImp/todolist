<?php

namespace App\NN\Helpers;

use App\NN\Interfaces\Task\ToDoListTaskHelperInterface;
use App\NN\Interfaces\Task\ToDoListTaskInterface;
use App\NN\Interfaces\Task\ToDoListTaskRepositoryInterface;
use App\NN\Services\Validation\Rules\TaskIsNotPastOverdue;
use App\NN\Services\Validation\Rules\TaskNotDisabled;
use App\NN\Services\Validation\Validators\TaskValidator;

/**
 * Helper service for manipulating ToDoListTask entity attributes
 * Class ToDoListTaskHelper
 * @package App\NN\Helpers
 */
class ToDoListTaskHelper implements ToDoListTaskHelperInterface
{
    /** @var ToDoListTaskRepositoryInterface */
    protected $repo;

    /**
     * ToDoListTaskHelper constructor.
     * @param ToDoListTaskRepositoryInterface $repo
     */
    public function __construct(ToDoListTaskRepositoryInterface $repo) {
        $this->repo = $repo;
    }

    /**
     * Filling ToDoListTask entity attributes
     * @param ToDoListTaskInterface $task
     * @param $data
     * @return ToDoListTaskInterface
     */
    public function fill(ToDoListTaskInterface $task, $data) {
        $task->setTitle($data['title'] ?? null);
        $task->setDescription($data['description'] ?? null);
        $task->setDeadline($data['deadline'] ?? null);
        $task->setDisabled($data['is_disabled'] ?? false);
        $task->setListId($data['list_id'] ?? $task->getListId());
        return $task;
    }

    /**
     * Update existing entity attributes or create a new one
     * @param $data
     * @return ToDoListTaskInterface|mixed
     */
    public function updateOrCreate($data) {
        $id = $data['id'] ?? null;
        $listId = $data['list_id'] ?? null;

        $task = !empty($id) ? $this->repo->getByIdAndListId($id, $listId) : resolve(ToDoListTaskInterface::class);

        $task = $this->fill($task, $data ?? []);

        $this->repo->save($task);

        return $task;
    }
}