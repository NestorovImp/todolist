<?php

namespace App\NN\Repositories;


use App\Models\ToDoListTask;
use App\NN\Exceptions\ToDoListException;
use App\NN\Interfaces\Base\BaseModelInterface;
use App\NN\Interfaces\Task\ToDoListTaskRepositoryInterface;


class EloquentToDoListTaskRepository implements ToDoListTaskRepositoryInterface
{

    protected $model;

    public function __construct(ToDoListTask $model)
    {
        $this->model = $model;
    }

    public function getById($id)
    {
        $task =  $this->model->find($id);

        if(is_null($task)) throw new ToDoListException("Can't find the task.");

        return $task;
    }

    public function getByIdAndListId($id, $listId)
    {
        $task =  $this->model->where('id', $id)->where('todo_list_id', $listId)->first();

        if(is_null($task)) throw new ToDoListException("Can't find the task that belong to the list.");

        return $task;
    }

    public function getTaskForListById($listId)
    {
        return $this->model->where('todo_list_id', $listId)->get();
    }

    public function getAll() {
        return $this->model->all();
    }

    public function save(BaseModelInterface $model)
    {
        $model->save();
    }

    public function delete(BaseModelInterface $model)
    {
        $model->delete();
    }

    public function deleteByTodoListId($listId) {
        $this->model->where('todo_list_id', $listId)->delete();
    }

}