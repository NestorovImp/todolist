<?php

namespace App\NN\Repositories;


use App\Models\ToDoList;
use App\NN\Exceptions\ToDoListException;
use App\NN\Interfaces\Base\BaseModelInterface;
use App\NN\Interfaces\ToDoList\ToDoListRepositoryInterface;


class EloquentToDoListRepository implements ToDoListRepositoryInterface
{

    protected $model;

    public function __construct(ToDoList $model)
    {
        $this->model = $model;
    }

    /**
     * @param $id
     * @return mixed
     * @throws ToDoListException
     */
    public function getById($id)
    {
        $list =  $this->model->find($id);

        if(is_null($list)) throw new ToDoListException("Can't find the list.");

        return $list;
    }

    public function getAll() {
        return $this->model->all();
    }

    public function save(BaseModelInterface $model)
    {
        $model->save();
    }

    public function delete(BaseModelInterface $model)
    {
        $model->delete();
    }

}