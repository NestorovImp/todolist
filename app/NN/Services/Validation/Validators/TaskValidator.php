<?php

namespace App\NN\Services\Validation\Validators;


use App\NN\Interfaces\Task\ToDoListTaskInterface;
use App\NN\Interfaces\Validation\TaskValidationRule;

/**
 * Class TaskValidator
 * @package App\NN\Services\Validation\Validators
 */
class TaskValidator
{

    /** @var ToDoListTaskInterface */
    protected $task;

    /**
     * TaskValidator constructor.
     * @param ToDoListTaskInterface $task
     */
    public function __construct(ToDoListTaskInterface $task)
    {
        $this->task = $task;
    }

    /**
     * Execute TaskValidationRule validation
     * @param TaskValidationRule ...$rules
     * @return bool
     */
    public function validate(TaskValidationRule ...$rules) {

        foreach ($rules as $rule) {
            $rule->validate($this->task);
        }
        return true;
    }

}