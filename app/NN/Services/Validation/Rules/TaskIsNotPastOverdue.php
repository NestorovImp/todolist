<?php

namespace App\NN\Services\Validation\Rules;

use App\NN\Exceptions\TaskValidationException;
use App\NN\Interfaces\Task\ToDoListTaskInterface;
use App\NN\Interfaces\Validation\TaskValidationRule;

/**
 * Class TaskIsNotPastOverdue
 * @package App\NN\Services\Validation\Rules
 */
class TaskIsNotPastOverdue implements TaskValidationRule
{

    public function validate(ToDoListTaskInterface $task)
    {
        if($task->isPastOverDue()) throw new TaskValidationException("Task is past overdue");
    }
}