<?php

namespace App\NN\Services\Validation\Rules;

use App\NN\Exceptions\TaskValidationException;
use App\NN\Interfaces\Task\ToDoListTaskInterface;
use App\NN\Interfaces\Validation\TaskValidationRule;

/**
 * Class TaskNotDisabled
 * @package App\NN\Services\Validation\Rules
 */
class TaskNotDisabled implements TaskValidationRule
{

    public function validate(ToDoListTaskInterface $task) {
        if($task->isDisabled()) throw new TaskValidationException("Task is disabled");
    }
}