<?php

namespace App\NN\Services\Validation\Rules;

use App\NN\Exceptions\TaskValidationException;
use App\NN\Interfaces\Task\ToDoListTaskInterface;
use App\NN\Interfaces\Validation\TaskValidationRule;

/**
 * Class TaskCompleted
 * @package App\NN\Services\Validation\Rules
 */
class TaskCompleted implements TaskValidationRule
{

    public function validate(ToDoListTaskInterface $task)
    {
        if($task->isCompleted()) throw new TaskValidationException("Task is completed");
    }
}