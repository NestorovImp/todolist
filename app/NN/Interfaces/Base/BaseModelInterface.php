<?php

namespace App\NN\Interfaces\Base;

/**
 * Interface BaseModelInterface
 * @package App\NN\Interfaces\Base
 */
interface BaseModelInterface
{
    /**
     * Get entity ID
     * @return mixed
     */
    public function getID();

    /**
     * Set entity title
     * @param $title
     * @return mixed
     */
    public function setTitle($title);

    /**
     * Get entity title
     * @return mixed
     */
    public function getTitle();

    /**
     * Set entity description
     * @param $description
     * @return mixed
     */
    public function setDescription($description);

    /**
     * Get entity description
     * @return mixed
     */
    public function getDescription();
}