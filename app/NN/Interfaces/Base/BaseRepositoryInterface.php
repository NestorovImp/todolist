<?php

namespace App\NN\Interfaces\Base;

/**
 * Interface BaseRepositoryInterface
 * @package App\NN\Interfaces\Base
 */
interface BaseRepositoryInterface
{
    /**
     * Get entity by ID
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * Get all entities
     * @return mixed
     */
    public function getAll();

    /**
     * Save entity
     * @param BaseModelInterface $model
     * @return mixed
     */
    public function save(BaseModelInterface $model);

    /**
     * Delete entity
     * @param BaseModelInterface $model
     * @return mixed
     */
    public function delete(BaseModelInterface $model);

}