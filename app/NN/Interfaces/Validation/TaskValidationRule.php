<?php

namespace App\NN\Interfaces\Validation;


use App\NN\Interfaces\Task\ToDoListTaskInterface;

/**
 * Interface TaskValidationRule
 * @package App\NN\Interfaces\Validation
 */
interface TaskValidationRule
{
    /**
     * Validating current task
     * @param ToDoListTaskInterface $task
     * @return mixed
     */
    public function validate(ToDoListTaskInterface $task);
}