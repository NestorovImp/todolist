<?php

namespace App\NN\Interfaces\ToDoList;


use App\NN\Interfaces\Base\BaseModelInterface;

interface ToDoListInterface extends BaseModelInterface
{
    /**
     * Get tasks related to list
     * @return mixed
     */
    public function getTasks();

    /**
     * Set list completed attribute
     * @param $completed
     * @return mixed
     */
    public function setCompleted($completed);

    /**
     * Set list completed attribute = true
     * @return mixed
     */
    public function markCompleted();

    /**
     * Set list completed_at attribute
     * @param $completedAt
     * @return mixed
     */
    public function setCompletedAt($completedAt);

    /**
     * Get lsit completed_at attribute
     * @return mixed
     */
    public function getCompletedAt();

    /**
     * Check if all related to list tasks are completed
     * @return mixed
     */
    public function isAllTasksCompleted();

}