<?php

namespace App\NN\Interfaces\ToDoList;

interface ToDoListHelperInterface
{

    public function fill(ToDoListInterface $list, $data);

    public function updateOrCreate($data);

    public function attachTasks($task, ToDoListInterface $list);

    public function markTaskCompleted($data);
}