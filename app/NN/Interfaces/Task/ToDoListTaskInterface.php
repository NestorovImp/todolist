<?php

namespace App\NN\Interfaces\Task;


use App\NN\Interfaces\Base\BaseModelInterface;

interface ToDoListTaskInterface extends BaseModelInterface
{
    /**
     *  Set task deadline attribute
     * @param $deadline
     * @return mixed
     */
    public function setDeadline($deadline);

    /**
     * Get task deadline attribute
     * @return mixed
     */
    public function getDeadline();

    /**
     * Set task completed attribute = true
     * @return mixed
     */
    public function markCompleted();

    /**
     * Set task completed attribute
     * @param $completed
     * @return mixed
     */
    public function setCompleted($completed);

    /**
     * Get task completed attribute
     * @return mixed
     */
    public function isCompleted();

    /**
     * Set task disabled attribute
     * @param $disabled
     * @return mixed
     */
    public function setDisabled($disabled);

    /**
     * Get task disabled attribute
     * @return mixed
     */
    public function isDisabled();

    /**
     * Set task list_id attribute
     * @param $listID
     * @return mixed
     */
    public function setListId($listID);

    /**
     * Get task list_id attribute
     * @return mixed
     */
    public function getListId();

    /**
     * Check if is taks is past overdue
     * @return mixed
     */
    public function isPastOverDue();
}