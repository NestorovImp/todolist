<?php

namespace App\NN\Interfaces\Task;


interface ToDoListTaskHelperInterface
{
    public function fill(ToDoListTaskInterface $list, $data);

    public function updateOrCreate($data);

}