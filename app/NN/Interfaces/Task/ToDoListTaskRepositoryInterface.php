<?php

namespace App\NN\Interfaces\Task;

use App\NN\Interfaces\Base\BaseRepositoryInterface;

interface ToDoListTaskRepositoryInterface extends BaseRepositoryInterface
{
    /**
     * Fetch all Tasks related to specific List
     * @param $listId
     * @return mixed
     */
    public function getTaskForListById($listId);

    /**
     * Fetch Task related to specific List
     * @param $listId
     * @return mixed
     */
    public function getByIdAndListId($id, $listId);

    /**
     * Delete all tasks related to List
     * @param $listId
     * @return mixed
     */
    public function deleteByTodoListId($listId);
}