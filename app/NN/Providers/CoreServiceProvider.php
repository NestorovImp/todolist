<?php

namespace App\NN\Providers;

use App\Models\ToDoList;
use App\Models\ToDoListTask;
use App\NN\Helpers\ToDoListTaskHelper;

use App\NN\Helpers\ToDoListHelper;

use App\NN\Interfaces\Task\ToDoListTaskInterface;
use App\NN\Interfaces\Task\ToDoListTaskRepositoryInterface;
use App\NN\Interfaces\Task\ToDoListTaskHelperInterface;
use App\NN\Interfaces\ToDoList\ToDoListHelperInterface;
use App\NN\Interfaces\ToDoList\ToDoListInterface;
use App\NN\Interfaces\ToDoList\ToDoListRepositoryInterface;
use App\NN\Repositories\EloquentToDoListRepository;
use App\NN\Repositories\EloquentToDoListTaskRepository;

use App\NN\Services\Validation\Rules\TaskIsNotPastOverdue;
use App\NN\Services\Validation\Rules\TaskNotCompleted;
use App\NN\Services\Validation\Rules\TaskNotDisabled;
use Illuminate\Support\ServiceProvider;

/**
 * Class CoreServiceProvider
 * @package App\NN\Providers
 */
class CoreServiceProvider extends ServiceProvider
{

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ToDoListRepositoryInterface::class, EloquentToDoListRepository::class);
        $this->app->bind(ToDoListTaskRepositoryInterface::class, EloquentToDoListTaskRepository::class);
        $this->app->bind(ToDoListHelperInterface::class, ToDoListHelper::class);
        $this->app->bind(ToDoListTaskHelperInterface::class, ToDoListTaskHelper::class);

        $this->app->bind(ToDoListTaskInterface::class, ToDoListTask::class);
        $this->app->bind(ToDoListInterface::class, ToDoList::class);

        $this->app->tag([TaskIsNotPastOverdue::class, TaskNotDisabled::class], ['taskEditRules', 'taskCompleteRules']);
        $this->app->tag([TaskNotCompleted::class], ['taskCompleteRules']);

    }

}