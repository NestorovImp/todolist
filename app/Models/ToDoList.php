<?php

namespace App\Models;

use App\NN\Interfaces\ToDoList\ToDoListInterface;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ToDoList
 * @package App\Models
 */
class ToDoList extends Model implements ToDoListInterface
{
    use SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'todo_lists';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id'
    ];



    public function tasks() {
        return $this->hasMany(ToDoListTask::class, 'todo_list_id');
    }

    public function getID() {
        return $this->id;
    }

    public function getTasks() {
        return $this->tasks;
    }

    public function setCompleted($completed)
    {
        $this->completed = $completed;
    }

    public function markCompleted() {
        $this->setCompleted(true);
        $this->setCompletedAt(Carbon::now());
    }

    public function setCompletedAt($completedAt) {
        $this->completed_at = $completedAt;
    }
    public function getCompletedAt() {
        return $this->completed_at;
    }


    public function setTitle($title) {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getDescription() {
        return $this->description;
    }

    public function isAllTasksCompleted() {
        $tasks = $this->getTasks();

        foreach ($tasks as $task) {
            if(!$task->isCompleted() && !$task->isDisabled()) return false;
        }

        return true;
    }
}
