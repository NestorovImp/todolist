<?php

namespace App\Models;

use App\NN\Interfaces\Task\ToDoListTaskInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * Class ToDoListTask
 * @package App\Models
 */
class ToDoListTask extends Model implements ToDoListTaskInterface
{
    use SoftDeletes;
    /**
     * @var string
     */
    protected $table = 'todo_list_tasks';

    public function getID() {
        return $this->id;
    }

    public function isCompleted() {
        return $this->completed;
    }

    public function setCompleted($completed) {
        $this->completed = $completed;
    }

    public function markCompleted() {
        $this->setCompleted(true);
    }

    public function isDisabled() {
        return $this->disabled;
    }

    public function setDisabled($disabled) {
        $this->disabled = $disabled;
    }

    public function setDeadline($deadline) {
        $this->deadline = $deadline;
    }

    public function getDeadline() {
        return $this->deadline;
    }

    public function setTitle($title) {
        $this->title = $title;
    }

    public function getTitle() {
        return $this->title;
    }

    public function setDescription($description) {
        $this->description = $description;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setListId($listID) {
        $this->todo_list_id = $listID;
    }

    public function getListId()  {
        return $this->todo_list_id;
    }

    public function isPastOverDue() {
        if(!empty($this->getDeadline()) && $this->getDeadline() < Carbon::now()) return true;

        return false;
    }
}
