<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('list', '')->group(function () {

    Route::get('', 'ToDoListController@index')->name('list.index.get');

    Route::get('/edit/{id?}', 'ToDoListController@edit')->name('list.edit.get');
    Route::post('/edit', 'ToDoListController@save')->name('list.edit.post');

    Route::post('/delete', 'ToDoListController@delete')->name('list.delete.post');

    Route::post('/complete/task', 'ToDoListController@markTaskCompleted')->name('list.complete.task.post');

});
Route::prefix('task')->group(function () {

    Route::post('/edit', 'TaskController@save')->name('task.edit.post');

    Route::post('/delete', 'TaskController@delete');
});


