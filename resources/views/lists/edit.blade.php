<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div>
                @if(!empty($erorrs))
                    {{$errors}}
                @endif
            </div>
            <div>
                <div>
                    <form method="POST" action="{{route('list.edit.post')}}">
                        @csrf
                        <div>
                            <input name="id" type="hidden" value="@if(!empty($list)) {{$list->getID()}}@endif">
                            <input type="text" name="title" placeholder="title" value="@if(!empty($list)){{$list->getTitle()}}@endif">
                            <input type="text" name="description" placeholder="description" value="@if(!empty($list)){{$list->getDescription()}}@endif">
                        </div>
                        <div>
                            @if(!empty($list))
                                @foreach($tasks as $task)
                                    <div>
                                        <input type="text" placeholder="title" value="{{$task->getTitle()}}" disabled>
                                        <input type="text" placeholder="description" value="{{$task->getDescription()}}" disabled>
                                        <input type="text" placeholder="deadline" value="{{$task->getDeadline()}}" disabled>
                                    </div>
                                @endforeach
                            @else
                                <div>
                                    <input name="tasks[0][title]" type="text" placeholder="title" value="">
                                    <input name="tasks[0][description]" type="text" placeholder="description" value="">
                                    <input name="tasks[0][deadline]" type="text" placeholder="deadline" value="">
                                </div>
                            @endif
                        </div>

                        <div>
                            <button type="submit">save</button>
                        </div>
                    </form>
                </div>
                <div>
                    @if(!empty($list))
                        <form method="POST" action="{{route('list.delete.post')}}">
                            <input name="id" type="hidden" value="{{$list->getID()}}">
                            <button type="submit">delete</button>
                        </form>
                    @endif
                </div>
            </div>

            <div>

            </div>
        </div>
    </body>
</html>
