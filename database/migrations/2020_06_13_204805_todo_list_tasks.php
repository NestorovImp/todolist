<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TodoListTasks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo_list_tasks', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('todo_list_id')->nullable(true)->default(null);
            $table->foreign('todo_list_id')->references('id')->on('todo_lists');
            $table->string('title')->nullable(false);
            $table->longText('description')->nullable(true)->default(null);
            $table->timestamp('deadline')->nullable(true)->default(null);
            $table->boolean('completed')->nullable(false)->default(false);
            $table->boolean('disabled')->nullable(false)->default(false);

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
